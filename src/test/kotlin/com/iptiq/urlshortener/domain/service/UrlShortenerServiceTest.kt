package com.iptiq.urlshortener.domain.service

import com.iptiq.urlshortener.UrlShortenerTestFixtures
import com.iptiq.urlshortener.adapter.repository.UrlShortenerInMemoryRepository
import com.iptiq.urlshortener.domain.exception.ShortenedUrlAlreadyExistsException
import com.iptiq.urlshortener.domain.exception.ShortenedUrlNotFoundException
import com.iptiq.urlshortener.domain.mapper.UrlShortenerMapper
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.kotlin.any
import org.mockito.kotlin.mock
import org.mockito.kotlin.times
import org.mockito.kotlin.verify
import org.mockito.kotlin.whenever

class UrlShortenerServiceTest {

    private val repository: UrlShortenerInMemoryRepository = mock {}
    private val service = UrlShortenerService(repository)

    @Test
    fun `should create ShortenedUrl with random hash successfully`() {
        val request = UrlShortenerTestFixtures.createUrlShortenerRequest()
        val shortenedUrl = UrlShortenerTestFixtures.createShortenedUrl()

        whenever(repository.save(any())).thenReturn(shortenedUrl)

        val result = service.createShortenedUrl(request)

        assertThat(result).isNotNull
        assertThat(result.shortenedUrl).isEqualTo(shortenedUrl.shortenedUrl)
        assertThat(result.originalUrl).isEqualTo(shortenedUrl.originalUrl)
    }

    @Test
    fun `should create ShortenedUrl with SEO keyword successfully`() {
        val seoKeyword = "test"
        val url = UrlShortenerMapper.BASE_URL + seoKeyword
        val request = UrlShortenerTestFixtures.createUrlShortenerRequest(seoKeyword = seoKeyword)
        val shortenedUrl = UrlShortenerTestFixtures.createShortenedUrl(shortenedUrl = url)

        whenever(repository.save(any())).thenReturn(shortenedUrl)

        val result = service.createShortenedUrl(request)

        assertThat(result).isNotNull
        assertThat(result.shortenedUrl).contains(seoKeyword)
        assertThat(result.originalUrl).isEqualTo(shortenedUrl.originalUrl)
    }

    @Test
    fun `should fail while creating ShortenedUrl due to url collision`() {
        val seoKeyword = "test"
        val url = UrlShortenerMapper.BASE_URL + seoKeyword
        val request = UrlShortenerTestFixtures.createUrlShortenerRequest(seoKeyword = seoKeyword)
        val shortenedUrl = UrlShortenerTestFixtures.createShortenedUrl(shortenedUrl = url)

        whenever(repository.save(any())).thenReturn(shortenedUrl)
        whenever(repository.findAllShortenedUrls()).thenReturn(listOf(shortenedUrl.shortenedUrl))

        val ex = assertThrows<ShortenedUrlAlreadyExistsException> {
            service.createShortenedUrl(request)
        }

        assertThat(ex).isInstanceOf(ShortenedUrlAlreadyExistsException::class.java)
    }

    @Test
    fun `should fetch ShortenedUrl by url address successfully`() {
        val url = "http://sho.com/test"
        val shortenedUrl = UrlShortenerTestFixtures.createShortenedUrl()

        whenever(repository.findByShortenedUrl(url)).thenReturn(shortenedUrl)

        val result = service.fetchShortenedUrlByUrlAddress(url)

        assertThat(result).isNotNull
        assertThat(result.shortenedUrl).isEqualTo(shortenedUrl.shortenedUrl)
        assertThat(result.originalUrl).isEqualTo(shortenedUrl.originalUrl)
    }

    @Test
    fun `should fail while fetching ShortenedUrl by url address due to not found`() {
        val url = "invalidUrl"

        whenever(repository.findByShortenedUrl(url)).thenReturn(null)

        val ex = assertThrows<ShortenedUrlNotFoundException> {
            service.fetchShortenedUrlByUrlAddress(url)
        }

        assertThat(ex).isInstanceOf(ShortenedUrlNotFoundException::class.java)
    }

    @Test
    fun `should not remove ShortenedUrl when it is still valid`() {
        val shortenedUrl = UrlShortenerTestFixtures.createShortenedUrl(validForHours = 3)

        whenever(repository.findAll()).thenReturn(listOf(shortenedUrl))

        service.removeOutOfDateShortenedUrls()

        verify(repository, times(0)).delete(shortenedUrl.shortenedUrl)
    }

    @Test
    fun `should remove ShortenedUrl when it is not valid anymore`() {
        val shortenedUrl = UrlShortenerTestFixtures.createShortenedUrl(validForHours = 0)
        Thread.sleep(1000L)

        whenever(repository.findAll()).thenReturn(listOf(shortenedUrl))

        service.removeOutOfDateShortenedUrls()

        verify(repository, times(1)).delete(shortenedUrl.shortenedUrl)
    }
}
