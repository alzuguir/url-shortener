package com.iptiq.urlshortener.domain.mapper

import com.iptiq.urlshortener.UrlShortenerTestFixtures
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

class UrlShortenerMapperTest {

    @Test
    fun mapCreateShortenedUrlRequestToShortedUrl() {
        val seoKeyword = "test"
        val request = UrlShortenerTestFixtures.createUrlShortenerRequest(
            url = "test",
            seoKeyword = seoKeyword,
            validForHours = 3
        )

        val result = UrlShortenerMapper.mapCreateShortenedUrlRequestToShortedUrl(request, request.seoKeyword ?: "")

        assertThat(result).isNotNull
        assertThat(result.shortenedUrl).contains(seoKeyword)
        assertThat(result.originalUrl).isEqualTo(request.url)
        assertThat(result.validForHours).isEqualTo(request.validForHours)
    }
}
