package com.iptiq.urlshortener.boundary.controller

import com.iptiq.urlshortener.UrlShortenerTestFixtures
import com.iptiq.urlshortener.boundary.AbstractIntegrationTest
import com.iptiq.urlshortener.boundary.dto.ShortenedUrlResponse
import com.iptiq.urlshortener.domain.mapper.UrlShortenerMapper
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

class UrlShortenerControllerTest : AbstractIntegrationTest() {

    @Test
    fun `Should POST UrlShortener successfully and return 201 status`() {
        val seoKeyword = "test"
        val request = UrlShortenerTestFixtures.createUrlShortenerRequest(seoKeyword = seoKeyword)

        val mvcResult = this.mockMvc.perform(
            MockMvcRequestBuilders
                .post("/url-shortener")
                .content(gson.toJson(request))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
        )

        val response = gson.fromJson(mvcResult.andReturn().response.contentAsString, ShortenedUrlResponse::class.java)

        mvcResult.andExpect(status().isCreated)
        assertThat(response).isNotNull
        assertThat(response.shortenedUrl).contains(seoKeyword)
        assertThat(response.originalUrl).isEqualTo(request.url)
    }

    @Test
    fun `Should fail while POST UrlShortener and return 409 status`() {
        val seoKeyword = "test"
        val request = UrlShortenerTestFixtures.createUrlShortenerRequest(seoKeyword = seoKeyword)

        this.mockMvc.perform(
            MockMvcRequestBuilders
                .post("/url-shortener")
                .content(gson.toJson(request))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
        )

        val mvcResult = this.mockMvc.perform(
            MockMvcRequestBuilders
                .post("/url-shortener")
                .content(gson.toJson(request))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
        )

        mvcResult.andExpect(status().isConflict)
    }

    @Test
    fun `Should GET UrlShortener by shortened url successfully and return 200 status`() {
        val seoKeyword = "test"
        val shortenedUrl = UrlShortenerMapper.BASE_URL + seoKeyword
        val request = UrlShortenerTestFixtures.createUrlShortenerRequest(seoKeyword = seoKeyword)

        this.mockMvc.perform(
            MockMvcRequestBuilders
                .post("/url-shortener")
                .content(gson.toJson(request))
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
        )

        val mvcResult = this.mockMvc.perform(
            MockMvcRequestBuilders
                .get("/url-shortener?url=$shortenedUrl")
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
        )

        val response = gson.fromJson(mvcResult.andReturn().response.contentAsString, ShortenedUrlResponse::class.java)

        mvcResult.andExpect(status().isOk)
        assertThat(response).isNotNull
        assertThat(response.shortenedUrl).isEqualTo(shortenedUrl)
        assertThat(response.originalUrl).isEqualTo(request.url)
    }

    @Test
    fun `Should fail while GET UrlShortener by shortened url and return 404 status`() {
        val missingUrl = "missingUrl"

        val mvcResult = this.mockMvc.perform(
            MockMvcRequestBuilders
                .get("/url-shortener?url=$missingUrl")
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
        )

        gson.fromJson(mvcResult.andReturn().response.contentAsString, ShortenedUrlResponse::class.java)

        mvcResult.andExpect(status().isNotFound)
    }

    @Test
    fun `Should fail while GET UrlShortener by shortened url when url parameter is missing and return 400 status`() {
        val mvcResult = this.mockMvc.perform(
            MockMvcRequestBuilders
                .get("/url-shortener")
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON)
        )

        gson.fromJson(mvcResult.andReturn().response.contentAsString, ShortenedUrlResponse::class.java)

        mvcResult.andExpect(status().isBadRequest)
    }
}
