package com.iptiq.urlshortener

import com.iptiq.urlshortener.boundary.dto.CreateUrlShortenerRequest
import com.iptiq.urlshortener.domain.domainobject.ShortenedUrl

object UrlShortenerTestFixtures {

    fun createUrlShortenerRequest(url: String = "", seoKeyword: String? = null, validForHours: Int = 1) =
        CreateUrlShortenerRequest(
            url = url,
            seoKeyword = seoKeyword,
            validForHours = validForHours
        )

    fun createShortenedUrl(url: String = "", shortenedUrl: String = "", validForHours: Int = 1) =
        ShortenedUrl(
            originalUrl = url,
            shortenedUrl = shortenedUrl,
            validForHours = validForHours
        )
}
