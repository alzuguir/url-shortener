package com.iptiq.urlshortener.adapter.repository

import com.iptiq.urlshortener.domain.domainobject.ShortenedUrl
import org.springframework.stereotype.Repository
import java.util.concurrent.ConcurrentHashMap

@Repository
class UrlShortenerInMemoryRepository {

    private val dataStore: MutableMap<String, ShortenedUrl> = ConcurrentHashMap()

    fun save(shortenedUrl: ShortenedUrl): ShortenedUrl =
        shortenedUrl.let { url ->
            dataStore[url.shortenedUrl] = url
            url
        }

    fun delete(key: String) =
        dataStore.remove(key)

    fun findByShortenedUrl(key: String): ShortenedUrl? =
        dataStore[key]

    fun findAllShortenedUrls(): List<String> =
        dataStore.keys.toList()

    fun findAll(): List<ShortenedUrl> =
        dataStore.values.toList()
}
