package com.iptiq.urlshortener.domain.domainobject

import com.iptiq.urlshortener.boundary.dto.ShortenedUrlResponse
import java.util.Date

data class ShortenedUrl(
    val originalUrl: String,
    val shortenedUrl: String,
    val validForHours: Int?,
    val createdAt: Date = Date()
)

fun ShortenedUrl.toDto() =
    ShortenedUrlResponse(
        originalUrl = this.originalUrl,
        shortenedUrl = this.shortenedUrl
    )
