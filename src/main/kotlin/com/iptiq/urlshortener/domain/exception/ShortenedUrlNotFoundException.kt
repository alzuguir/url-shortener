package com.iptiq.urlshortener.domain.exception

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ResponseStatus

@ResponseStatus(HttpStatus.NOT_FOUND)
class ShortenedUrlNotFoundException(override val message: String) : RuntimeException(message)
