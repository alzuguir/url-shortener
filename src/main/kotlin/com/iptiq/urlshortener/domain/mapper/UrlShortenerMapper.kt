package com.iptiq.urlshortener.domain.mapper

import com.iptiq.urlshortener.boundary.dto.CreateUrlShortenerRequest
import com.iptiq.urlshortener.domain.domainobject.ShortenedUrl

object UrlShortenerMapper {

    const val BASE_URL = "http://sho.com/"

    fun mapCreateShortenedUrlRequestToShortedUrl(
        request: CreateUrlShortenerRequest,
        urlSuffix: String
    ): ShortenedUrl =
        ShortenedUrl(
            originalUrl = request.url,
            shortenedUrl = BASE_URL + urlSuffix,
            validForHours = request.validForHours
        )
}
