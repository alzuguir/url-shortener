package com.iptiq.urlshortener.domain.service

import com.iptiq.urlshortener.adapter.repository.UrlShortenerInMemoryRepository
import com.iptiq.urlshortener.boundary.dto.CreateUrlShortenerRequest
import com.iptiq.urlshortener.boundary.dto.ShortenedUrlResponse
import com.iptiq.urlshortener.domain.domainobject.ShortenedUrl
import com.iptiq.urlshortener.domain.domainobject.toDto
import com.iptiq.urlshortener.domain.exception.ShortenedUrlAlreadyExistsException
import com.iptiq.urlshortener.domain.exception.ShortenedUrlNotFoundException
import com.iptiq.urlshortener.domain.mapper.UrlShortenerMapper
import mu.KotlinLogging
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import java.util.Calendar
import java.util.Date

@Service
class UrlShortenerService(private val urlShortenerRepository: UrlShortenerInMemoryRepository) {

    private val logger = KotlinLogging.logger {}

    fun createShortenedUrl(request: CreateUrlShortenerRequest): ShortenedUrlResponse =
        UrlShortenerMapper.mapCreateShortenedUrlRequestToShortedUrl(
            request, generateUrlSuffix(request)
        ).let { url ->
            checkForCollision(url.shortenedUrl)
            urlShortenerRepository.save(url).toDto()
        }.also { shortenedUrl -> logger.debug { "Shortened URL added successfully ${shortenedUrl.shortenedUrl}" } }

    fun fetchShortenedUrlByUrlAddress(url: String): ShortenedUrlResponse =
        urlShortenerRepository.findByShortenedUrl(url)?.toDto()
            ?: throw ShortenedUrlNotFoundException("Shortened URL: $url was not found")

    @Scheduled(fixedRate = 10000)
    fun removeOutOfDateShortenedUrls() = urlShortenerRepository.findAll().forEach { shortenedUrl ->
        if (shortenedUrl.validForHours != null) {
            if (Date() > getShortenedUrlValidDate(shortenedUrl))
                urlShortenerRepository.delete(shortenedUrl.shortenedUrl)
                    .also { logger.debug { "Shortened URL: $shortenedUrl expired and was removed successfully" } }
        }
    }

    private fun getShortenedUrlValidDate(shortenedUrl: ShortenedUrl): Date = Calendar.getInstance().let { calendar ->
        calendar.time = shortenedUrl.createdAt
        shortenedUrl.validForHours?.let { hours -> calendar.add(Calendar.HOUR_OF_DAY, hours) }
        calendar.time
    }

    private fun checkForCollision(shortenedUrl: String) =
        urlShortenerRepository.findAllShortenedUrls().let { allShortenedUrls ->
            if (allShortenedUrls.contains(shortenedUrl)) {
                throw ShortenedUrlAlreadyExistsException("A shortened URL with the same address: $shortenedUrl already exists.")
            }
        }

    private fun generateUrlSuffix(request: CreateUrlShortenerRequest): String =
        if (request.seoKeyword.isNullOrEmpty()) createUrlHash()
        else request.seoKeyword

    private fun createUrlHash(): String = (('0'..'9') + ('a'..'z') + ('A'..'Z')).let { hashChars ->
        (1..6).map { kotlin.random.Random.nextInt(0, hashChars.size) }
            .map(hashChars::get)
            .joinToString("")
            .uppercase()
    }
}
