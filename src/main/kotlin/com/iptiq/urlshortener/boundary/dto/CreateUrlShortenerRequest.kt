package com.iptiq.urlshortener.boundary.dto

data class CreateUrlShortenerRequest(
    val url: String,
    val seoKeyword: String?,
    val validForHours: Int?
)
