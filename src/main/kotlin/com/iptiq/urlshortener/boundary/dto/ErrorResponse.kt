package com.iptiq.urlshortener.boundary.dto

data class ErrorResponse(
    val status: Int,
    val code: ExceptionCode,
    val message: String
)

enum class ExceptionCode {
    NOT_FOUND, VALIDATION_ERROR, INTERNAL_SERVER_ERROR
}
