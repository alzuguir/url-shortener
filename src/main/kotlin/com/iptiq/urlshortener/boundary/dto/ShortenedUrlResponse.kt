package com.iptiq.urlshortener.boundary.dto

data class ShortenedUrlResponse(
    val originalUrl: String,
    val shortenedUrl: String
)
