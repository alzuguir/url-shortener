package com.iptiq.urlshortener.boundary.controller

import com.iptiq.urlshortener.boundary.dto.CreateUrlShortenerRequest
import com.iptiq.urlshortener.boundary.dto.ShortenedUrlResponse
import com.iptiq.urlshortener.domain.service.UrlShortenerService
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/url-shortener")
class UrlShortenerController(private val urlShortenerService: UrlShortenerService) {

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun postShortenedUrl(@RequestBody request: CreateUrlShortenerRequest): ShortenedUrlResponse =
        urlShortenerService.createShortenedUrl(request)

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    fun getShortenedUrlByUrlAddress(@RequestParam(required = true) url: String): ShortenedUrlResponse? =
        urlShortenerService.fetchShortenedUrlByUrlAddress(url)
}
