package com.iptiq.urlshortener.boundary.controller

import com.iptiq.urlshortener.boundary.dto.ErrorResponse
import com.iptiq.urlshortener.boundary.dto.ExceptionCode
import com.iptiq.urlshortener.domain.exception.ShortenedUrlAlreadyExistsException
import com.iptiq.urlshortener.domain.exception.ShortenedUrlNotFoundException
import mu.KotlinLogging
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.context.request.WebRequest

@ControllerAdvice
class UrlShortenerControllerAdvice {

    private val logger = KotlinLogging.logger {}

    @ExceptionHandler(value = [ShortenedUrlAlreadyExistsException::class])
    fun handleShortenedUrlAlreadyExistsException(
        ex: ShortenedUrlAlreadyExistsException,
        request: WebRequest
    ): ResponseEntity<ErrorResponse> =
        ResponseEntity(
            ErrorResponse(
                status = HttpStatus.CONFLICT.value(),
                message = ex.message,
                code = ExceptionCode.VALIDATION_ERROR
            ),
            HttpStatus.CONFLICT
        ).also { logger.error(ex) { "Error while trying to create shortened URL." } }

    @ExceptionHandler(value = [ShortenedUrlNotFoundException::class])
    fun handleShortenedUrlNotFoundException(
        ex: ShortenedUrlNotFoundException,
        request: WebRequest
    ): ResponseEntity<ErrorResponse> =
        ResponseEntity(
            ErrorResponse(
                status = HttpStatus.NOT_FOUND.value(),
                message = ex.message,
                code = ExceptionCode.NOT_FOUND
            ),
            HttpStatus.NOT_FOUND
        ).also { logger.debug(ex) { "Shortened URL not found." } }

    @ExceptionHandler(value = [RuntimeException::class])
    fun handleRuntimeException(
        ex: RuntimeException,
        request: WebRequest
    ): ResponseEntity<ErrorResponse> =
        ResponseEntity(
            ErrorResponse(
                status = HttpStatus.INTERNAL_SERVER_ERROR.value(),
                message = ex.message ?: "",
                code = ExceptionCode.INTERNAL_SERVER_ERROR
            ),
            HttpStatus.INTERNAL_SERVER_ERROR
        ).also { logger.error(ex) { "Internal server error." } }
}
