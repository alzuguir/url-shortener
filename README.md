# URL Shortener Service
This is a microservice with REST APIs developed to shorten URLs according to the specifications requested in the code assignment.

## Stack
- Kotlin v1.6.10
- Spring Boot v2.6.6
- Maven
- JUnit 5
- Kotlin Mockito

## Running Instructions
The service was developed using Spring Boot and IntelliJ (you can run through the terminal or the IDE of your choice by executing the main method in the UrlShortenerApplication class). 

To run a full build and download all necessary dependencies please execute `mvn clean install` command.

It will be executed and run into the port 8080 as default configuration.

## Development Process
The service was developed taking into consideration some paradigms as domain driven design, hexagonal architecture, full test coverage, and data transfer objects to isolate our domain data.

From a test perspective unit tests were implemented on the Service layer covering all lines and edge cases, integration tests are implemented using Spring and focus on testing the API and its response codes.
An in memory storage was implemented as requested on the assignment

ConcurrentHashMap was the data structure chosen for the data store because of its thread safe capabilities and improved performance when compared to a regular HashMap or using synchronized methods.

## Task 1
To fulfil the task one requirements we have a POST route in the UrlShortenerController (`/url-shortener`), where we take an optional parameter called `seoKeyword` on the request payload, if the keyword is present we shorten the URL using our domain base path plus the keyword.

## Task 2
The task 2 implementation depends on the task 1, and if no SEO keyword is provided in the request payload for the POST resource a hash of 6 characters containing letters and numbers are generated as part of our shortened URL.

## Task 3
For the task 3 implementation a GET route  (`/url-shortener`) was implemented with a required `url` parameter, if the shortened URL is found it will be returned, in case it is not found the API should return 404 and in case the parameter is not provided the API will return 400 as response status.

## Task 4
For the task 4 implementation we check in the data store if the shortened URL matches a previously created one before persisting it in our memory data store, in case it already exists the API should return status code 409 for the POST creation request.

## Task 5
To support the time to live feature, in the moment we create a shortened URL an optional parameter `validForHours` can be provided in our request payload, this parameters will determine how many hours of validity our shorted URL has, if the parameter is blank it will never be deleted, if the parameter is provided we have a scheduled job using Spring Scheduler that will check the moment of creation, the validForHours and the current date to determine if it should be deleted from the data store or not.

________
Author: Felippe Alzuguir

For any questions please contact: felippealzuguir@gmail.com